'''
Created on 18 Jan 2014

@author: Gabriel
'''

import win32api, win32con
class detectKeyboardInput(): # Collection of methods which detect keyboard input. 
    # Utility functions which enables adjustment of things in real-time using keyboard input.    
    def isAltKeyPressed(self):
        return (win32api.GetKeyState(win32con.VK_LMENU) == -127 or win32api.GetKeyState(win32con.VK_LMENU) == -128) # Evaluates to true if the alt key is pressed

    def isShiftKeyPressed(self):
        return (win32api.GetKeyState(win32con.VK_SHIFT) == -127 or win32api.GetKeyState(win32con.VK_SHIFT) == -128) 
        
    def isQPressed(self):
        return (win32api.GetKeyState(0x51) == -127 or win32api.GetKeyState(0x51) == -128)
        
    def isWPressed(self):
        return (win32api.GetKeyState(0x57) == -127 or win32api.GetKeyState(0x57) == -128)
        
    def isEPressed(self):
        return (win32api.GetKeyState(0x45) == -127 or win32api.GetKeyState(0x45) == -128)
   
    def isRPressed(self):
        return (win32api.GetKeyState(0x52) == -127 or win32api.GetKeyState(0x52) == -128)  