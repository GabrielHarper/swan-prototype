'''
Created on 4 May 2014

@author: Gabriel
'''

import win32com.client, win32gui
autoit = win32com.client.Dispatch("AutoItX3.Control") # Enables referencing of autoit methods, e.g. autoit.Send()
diabloWindowHandle = win32gui.FindWindow(None,'Diablo II') # Enables module to get the Diablo window.
class ConvertCoordinates(object):
    # This will convert coordinates from being relative to the window, to being relative to the global resolution. 
    def convertCoordinatesToRelativeToWindow(self, coordinates, windowHandle = diabloWindowHandle):
        windowX,windowY,__,__ = win32gui.GetWindowRect(windowHandle)
        convertedCoords = (windowX + coordinates[0], windowY + coordinates[1])
        return convertedCoords
    def __init__(self):
        '''
        Constructor
        '''
        