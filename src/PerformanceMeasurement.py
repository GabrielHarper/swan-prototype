
import cv2

# Returns the number of seconds taken to execute the function on average.
def timeIt(f, iterations, params):
    numCalls = 0
    totalTime = 0
    for _ in range(0,iterations):
        timeOne = cv2.getTickCount()
        if params is (): # if the tuple is empty, call it without any arguments
            f()
        else:
            f(*params)
        timeTwo = cv2.getTickCount()
        numCalls = numCalls + 1
        timeToCall = (timeTwo - timeOne) / cv2.getTickFrequency()
        totalTime = totalTime + timeToCall
        averageTime = totalTime / numCalls
    return averageTime
    