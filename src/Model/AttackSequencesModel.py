'''
Created on 2 Jun 2014

@author: Gabriel
'''
import win32com.client
import win32gui
import Globals.Globals
autoit = win32com.client.Dispatch("AutoItX3.Control") # Enables referencing of autoit methods, e.g. autoit.Send()
diabloWindowHandle = Globals.Globals.diabloWindowHandle # Enables module to get the Diablo window.

def getClearScreenAttackSequence(): 
    left,top,right,bottom = win32gui.GetWindowRect(diabloWindowHandle)
    height = bottom - top
    width = right - left
    centreOfDiabloScreen = [left + (width / 2), top + (height / 2)]
    width = 800
    height = 600
    
    leftAttack = [centreOfDiabloScreen[0] - (width / 5), centreOfDiabloScreen[1]]
    rightAttack = [centreOfDiabloScreen[0] + (width / 5), centreOfDiabloScreen[1]]
    topAttack = [centreOfDiabloScreen[0], centreOfDiabloScreen[1] - (height / 5)]
    bottomAttack = [centreOfDiabloScreen[0], centreOfDiabloScreen[1] + (height / 5)]
    
    # This attack sequence is a sequence of coordinates, which are slightly to the left, right top and bottom of the character.
    # It is mainly used for casting aoe spells in all directions, in order to clear a screen.
    clearScreenAttackSequence = [leftAttack,topAttack,rightAttack,bottomAttack]
    return clearScreenAttackSequence