'''
Created on 25 Apr 2014

@author: Gabriel
'''

from enum import Enum


class potionSetups(Enum):
    #These are the various setups for each column of the belt where each element refers to the column.
    ALLHEALTH = ["HEALTH","HEALTH","HEALTH","HEALTH"]
    ALLMANA = ["MANA", "MANA", "MANA", "MANA"]
    HALFHALF = ["HEALTH","HEALTH","MANA","MANA"]
    HEALTHMANAREJUVENATION = ["HEALTH","HEALTH","MANA","REJUVENATION"]
    
class PotionsModel():
    '''
        This is used to represent the potions in the player's belt.
    '''
    # Upon initialization, all potions are set to be fill.
    def __init__(self, potionSetup):
        self.potionsPerBeltColumn = 3 # This is currently hardcoded. In the future, this data can be retrieved from Diablo via memory injection or computer vision.
        self.potionSetup = potionSetup
        self.columnOne, self.columnTwo, self.columnThree, self.columnFour = [],[],[],[] # The columns of each belt are lists consisting of potions.
        self.belt = self.columnOne, self.columnTwo, self.columnThree, self.columnFour
        for _ in range(self.potionsPerBeltColumn): # Add the appropriate number of potions in each column
            for colIndex,column in list(enumerate(self.belt)):
                column.append(potionSetup.value[colIndex]) 
    def refillPotions(self): #Sets all the potion slots to be full of the potion type specified.
        for colIndex, column in list(enumerate(self.belt)):
            for potIndex,_ in list(enumerate(column)):
                self.belt[colIndex][potIndex] = self.potionSetup.value[colIndex]
                     
    def addPotionsListener(self):
        ''' 
        TODO
        class documentation not currently implemented
        '''
        pass
    def getNumberOfHealthPotions(self):
        ''' 
        TODO
        class documentation not currently implemented
        '''
        pass
    def getNumberOfManaPotions(self):
        ''' 
        TODO
        class documentation not currently implemented
        '''
        pass
    def getNumberOfRejuvenationPotions(self):
        ''' 
        TODO
        class documentation not currently implemented
        '''
        pass
    
