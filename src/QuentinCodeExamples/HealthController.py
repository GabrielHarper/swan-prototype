
'''

Quentin's example of how to structure this according to MVC model:


new HealthController(
healthSensor: new ImageComparisonHealthSensor(),
 whenOutOfPotions: function () { 
     MessageSender.alert("Out of potions! Taking town portal"); 
     PortalTaker.takeTownPortal(); 
 }, 
 potionDrinker: new HotkeyPotionDrinker()
)


Health Controller constructor:

potionsModel = new PotionsModel()
potionsSpotter = new PotionsSpotter();
potionsDrinker = new HotkeyPotionDrinker(potionsModel);
potionsModel.addPotionsChangeListener(function (model) {
   if (model.getHealthPotions().size() == 0) { whenOutOfPotions(); }
});

healthSensor = new ImageComparisonHealthSensor();
healthSensor.addHealthChangeListener(function (healthModel) {
  if (healthModel.getHealth() < 0.5 && healthModel.getHealthChange() < 0) { potionsDrinker.drinkHealthPotion(1.0 - health); }
})

Health Controller tick method:

def tick(Image screenshot):
    healthSensor.tick(screenshot);
    potionsSpotter.updatePotionsModel(potionsModel, screenshot);

Diablo Bot tick method:
'''
'''
Created on 2 Jun 2014

@author: Gabriel
'''
