'''
Created on 4 May 2014

@author: Gabriel
'''
import win32com.client
autoit = win32com.client.Dispatch("AutoItX3.Control") # Enables referencing of autoit methods, e.g. autoit.Send()
class AttackSequences(object):
    '''
    classdocs
    '''

    def __init__(self):
        autoit.Send("#3")
        '''
        Constructor
        '''
        
    def rightClickAttack(self, coordinates):
        autoit.MouseClick("right",coordinates[0],coordinates[1])