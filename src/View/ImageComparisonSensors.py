'''
Created on 25 Apr 2014

@author: Gabriel
'''
import cv2, win32api
from Utility.detectKeyboardInputs import detectKeyboardInput
from View import DisplayFrame
displayFrame = DisplayFrame.DisplayFrame()
import Globals.Globals

# Depending on the arguments passed to the constructor, this class will act as a sensor for health or mana.
# Similar to other tick methods, this class takes a frame of the Diablo II window as an argument.



class ImageComparisonSensor(object):
    def __init__(self, globe):
        # If isHealthSensor is false, then this sensor acts as a sensor for mana.
        detectInput = detectKeyboardInput()
        self.globe = globe
            # The following variables are hard-coded, and were obtained after much fiddling around with healthbars!  They are used in the get_currentHealthImage function.
        if (globe == Globals.Globals.globeType.HEALTH):
            self.healthInitializer()
        elif(globe ==  Globals.Globals.globeType.MANA):
            self.manaInitializer()
        else:
            print("You must pass in a valid enum value of globeType.HEALTH or globeType.MANA")
                             
    '''
     Gets a rectangular image of the current health bar, using hard-coded ratios. When the bot starts, a small sample of this image will be used to determine what full health should look like. 
     It is fiddly to get this image to capture the health bar perfectly, so the actual image will have overlap of say, the ground/menu bar. 
     Furthermore, this image may not always be resolution independent due to rounding.
     This is why this function should be called each time the bot runs to determine what full health should look like, rather than using a saved image. 
    
     To determine what full health should look like, it should only be called once, directly after healing the character to full health.
     
     After this point, it can be used to get an image of the current player's health.
     
     The hard-coded ratios with overlap  of this function and _extractSampleOfImage account for a absolute error of 16 percentage points. i.e. actual 0% health appears as 16% to the bot. 
    
    
    
    '''
    def manaInitializer(self):
        self.absolutePathSampleImage = 'C:/Users/Gabriel/workspace/DiabloBlackSwan/src/images/health/fullManaSample.jpg' 
        relativePath = '../../images/health/fullManaSample.jpg' #TODO
        self.fullSampleImage = cv2.imread(self.absolutePathSampleImage) # use this as a sample of full health - this can always be changed later through the setSampleHealthImage method.

    def healthInitializer(self):
        self.absolutePathSampleImage = 'C:/Users/Gabriel/workspace/DiabloBlackSwan/src/images/health/fullHealthSample.jpg'
        relativePath = '../../images/health/fullHealthSample.jpg'
        self.fullSampleImage = cv2.imread(self.absolutePathSampleImage) # use this as a sample of full health - this can always be changed later through the setSampleHealthImage method.

            
    def setNewFullSample(self, sample):
        self.fullSampleImage = sample
        
    def _extractImage(self, diabloScreenshot): # These hard-coded ratios mess up when I move the Diablo window about. :S *REQUIRES FIXING*
        height,width,_ = diabloScreenshot.shape     
        # These variables are hard-coded ratios, which are used to obtain the area of the image which contains the health bar.
        # The current variables will return the 3-15%(left-right) and 85-100%(top-bottom) of the image.
        if (self.globe == Globals.Globals.globeType.HEALTH):
            leftCoordHealthImage = int (width*0.03)
            bottomCoordHealthImage = int (height * 1.0)
            topCoordHealthImage = int(height * 0.85)
            rightCoordHealthImage = int(width * 0.15)
            image = diabloScreenshot[topCoordHealthImage:bottomCoordHealthImage,leftCoordHealthImage:rightCoordHealthImage]        
        else:
            leftCoordManaImage = int (width*0.85)
            bottomCoordManaImage = int (height * 1.0)
            topCoordManaImage = int(height * 0.85)
            rightCoordManaImage = int(width * 0.97)
            image = diabloScreenshot[topCoordManaImage:bottomCoordManaImage,leftCoordManaImage:rightCoordManaImage]        
        return image  
    '''
    This will take a tiny vertical one pixel wide sample of a given health image, using hard coded ratios. 
    Consequently, this function is dependent upon @_extractHealthImage().
    '''
    def _extractSampleOfImage(self, image): 
        heightOfImage = image.shape[0]
        widthOfImage = image.shape[1]
        middleOfImage = widthOfImage / 2.0 # Takes the middleish of the image - this should be directly within the sphere. 
        sample = image[(heightOfImage * 0.05):(heightOfImage * 0.95),middleOfImage:middleOfImage+1] # Defines a one-pixel wide Region Of Interest in the middle of the image with 80% of the height(excludes top and bottom 10%).
        return sample
    
    '''
    This compares two sample images to determine how similar they are.
    By comparing each pixel, it returns a percentage of the player's current health
    '''
    def _compareSamples(self, sampleOne,sampleTwo):
        matchingPixels = 0
        totalPixels = 0
        for index,pixel in list(enumerate(sampleOne)):
            
            # Leave a sensitivity threshold of magnitude ten each way.
            magnitude = 5
            if (pixel[0][0] < sampleTwo[index][0][0] + magnitude and pixel[0][0] > sampleTwo[index][0][0] - magnitude ):
                redPixelMatch = True
            else:
                redPixelMatch = False
                        
            if (pixel[0][1] < sampleTwo[index][0][1] + magnitude and pixel[0][1] > sampleTwo[index][0][1] - magnitude ):
                greenPixelMatch = True
            else:
                greenPixelMatch = False
                
                
            if (pixel[0][2] < sampleTwo[index][0][2] + magnitude and pixel[0][2] > sampleTwo[index][0][2] - magnitude ):
                bluePixelMatch = True
            else:
                bluePixelMatch = False
            if (redPixelMatch and greenPixelMatch and bluePixelMatch ): # If the red, green and blue elements of a pixel in sampleOne match the pixel in sampleTwo, then the entire pixel matches. 
                matchingPixels +=1
            totalPixels += 1
        percentage = ( (float(matchingPixels) / float(totalPixels)) * 100)
        return percentage
    
    def _takeSample(self, diabloScreenshot):
        image = self._extractImage(diabloScreenshot)
        sample = self._extractSampleOfImage(image)
        return sample
    
    
    def takeNewFullSample(self, diabloScreenshot):
        newSample = self._takeSample(diabloScreenshot)
        success = cv2.imwrite(self.absolutePathSampleImage, newSample)
        if(success):
            print("A new sample for " + self.globeType + " has been successfully taken")
        else:
            print("Failed to take" + self.globeType + " sample.")
        
    #This will be the main method which is called from this class.'
    def getGlobePercentage(self, diabloScreenshot):
        percentage = self._compareSamples(self._takeSample(diabloScreenshot),self.fullSampleImage) 
        #update health percentage
        return percentage # return health percentage

class ImageComparisonHealthSensor(object):
    ''' 
    This class takes a stripped out image of a Diablo II window as an argument.
    It will then find the health bar within this image and return the current health. 
    class documentation not currently implemented
    '''
    def __init__(self): # Upon instantiation, waits for user to take full health sample.
        detectInput = detectKeyboardInput()
        # The following variables are hard-coded, and were obtained after much fiddling around with healthbars!  They are used in the get_currentHealthImage function.
        #
        self.absolutePathSampleImage = 'C:/Users/Gabriel/workspace/DiabloBlackSwan/src/images/health/fullHealthSample.jpg'
        relativePath = '../../images/health/fullHealthSample.jpg'
        self.fullHealthSampleImage = cv2.imread(self.absolutePathSampleImage) # use this as a sample of full health - this can always be changed later through the setSampleHealthImage method.
                
    '''
     Gets a rectangular image of the current health bar, using hard-coded ratios. When the bot starts, a small sample of this image will be used to determine what full health should look like. 
     It is fiddly to get this image to capture the health bar perfectly, so the actual image will have overlap of say, the ground/menu bar. 
     Furthermore, this image may not always be resolution independent due to rounding.
     This is why this function should be called each time the bot runs to determine what full health should look like, rather than using a saved image. 
    
     To determine what full health should look like, it should only be called once, directly after healing the character to full health.
     
     After this point, it can be used to get an image of the current player's health.
     
     The hard-coded ratios with overlap  of this function and _extractSampleOfImage account for a absolute error of 16 percentage points. i.e. actual 0% health appears as 16% to the bot. 
    
    
    
    '''
            
    def setNewFullHealthSample(self, sample):
        self.fullHealthSampleImage = sample
        
        
    def _extractHealthImage(self, diabloScreenshot): # These hard-coded ratios mess up when I move the Diablo window about. :S *REQUIRES FIXING*
        height,width,_ = diabloScreenshot.shape     
        # These variables are hard-coded ratios, which are used to obtain the area of the image which contains the health bar.
        # The current variables will return the 3-15%(left-right) and 85-100%(top-bottom) of the image.
        leftCoordHealthImage = int (width*0.03)
        bottomCoordHealthImage = int (height * 1.0)
        topCoordHealthImage = int(height * 0.85)
        rightCoordHealthImage = int(width * 0.15)
        healthImage = diabloScreenshot[topCoordHealthImage:bottomCoordHealthImage,leftCoordHealthImage:rightCoordHealthImage]        
        return healthImage  
    '''
    This will take a tiny vertical one pixel wide sample of a given health image, using hard coded ratios. 
    Consequently, this function is dependent upon @_extractHealthImage().
    '''
    def _extractSampleOfImage(self, healthImage): 

        heightOfHealthImage = healthImage.shape[0]
        widthOfHealthImage = healthImage.shape[1]
        middleOfHealthImage = widthOfHealthImage / 2.0 # Takes the middleish of the image - this should be directly within the health sphere. 
        healthSample = healthImage[(heightOfHealthImage * 0.05):(heightOfHealthImage * 0.95),middleOfHealthImage:middleOfHealthImage+1] # Defines a one-pixel wide Region Of Interest in the middle of the image with 80% of the height(excludes top and bottom 10%).
        return healthSample
    
    '''
    This compares two sample images to determine how similar they are.
    By comparing each pixel, it returns a percentage of the player's current health
    '''
    def _compareSamples(self, sampleOne,sampleTwo):
        matchingPixels = 0
        totalPixels = 0
        for index,pixel in list(enumerate(sampleOne)):
            
            # Leave a sensitivity threshold of magnitude ten each way.
            magnitude = 5
            if (pixel[0][0] < sampleTwo[index][0][0] + magnitude and pixel[0][0] > sampleTwo[index][0][0] - magnitude ):
                redPixelMatch = True
            else:
                redPixelMatch = False
                        
            if (pixel[0][1] < sampleTwo[index][0][1] + magnitude and pixel[0][1] > sampleTwo[index][0][1] - magnitude ):
                greenPixelMatch = True
            else:
                greenPixelMatch = False
                
                
            if (pixel[0][2] < sampleTwo[index][0][2] + magnitude and pixel[0][2] > sampleTwo[index][0][2] - magnitude ):
                bluePixelMatch = True
            else:
                bluePixelMatch = False
            '''
            redPixelMatch = (pixel[0][0] == sampleTwo[index][0][0])
            greenPixelMatch = (pixel[0][1] == sampleTwo[index][0][1])
            bluePixelMatch = (pixel[0][2] == sampleTwo[index][0][2])
            '''
            if (redPixelMatch and greenPixelMatch and bluePixelMatch ): # If the red, green and blue elements of a pixel in sampleOne match the pixel in sampleTwo, then the entire pixel matches. 
                matchingPixels +=1
            totalPixels += 1
        percentage = ( (float(matchingPixels) / float(totalPixels)) * 100)
        return percentage
    
    def _takeSample(self, diabloScreenshot):
        healthImage = self._extractHealthImage(diabloScreenshot)
        healthSample = self._extractSampleOfImage(healthImage)
        return healthSample
    
    
    def takeNewFullSample(self, diabloScreenshot):
        newSample = self._takeSample(diabloScreenshot)
        success = cv2.imwrite(self.absolutePathSampleImage, newSample)
        if(success):
            print("A new sample has been successfully taken")
        else:
            print("Failed to take health sample.")
        
    #This will be the main method which is called from this class.'
    def getGlobePercentage(self, diabloScreenshot):
        healthPercentage = self._compareSamples(self._takeSample(diabloScreenshot),self.fullHealthSampleImage) 
        #update health percentage
        return healthPercentage # return health percentage
    

    