'''
Created on 11 Jun 2014

@author: Gabriel
'''
from ImageManipulation import ImageCapture, ImageDetection, ConvertImage
import cv2
import win32api, win32com.client
autoit = win32com.client.Dispatch("AutoItX3.Control") # Enables referencing of autoit methods, e.g. autoit.Send()
screenshotter = ImageCapture.ScreenShotter()


def getMap():
    _,withoutMap = screenshotter.screenshot()
    autoit.Send("{TAB}")
    win32api.Sleep(500)
    _,withMap = screenshotter.screenshot()
    diabloMap = cv2.subtract(withoutMap,withMap)
    return diabloMap


def getPlaceNameROI(diabloMap):
    height = diabloMap.shape[0]
    width = diabloMap.shape[1]
    return diabloMap[height*0.05:height*0.15,width*0.6:width] # return the top 95-85% region and the 30% right region. 

def KNNTextRecognition(diabloMap):
    pass

def getDiabloFont():
    #win32api.get
    
    
    