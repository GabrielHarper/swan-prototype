
'''
Created on 26 Jan 2014

@author: Gabriel

Todo:
- Refine images to be of Diablo window.
- Feed these images into the tests.

'''
import unittest, cv2, os
from View import ImageComparisonSensors
from ImageManipulation import ImageDetection
from ImageManipulation import ConvertImage
healthSensitivityThreshold = 20

class HealthSensorTestCases(unittest.TestCase):
    healthSensor = ImageComparisonSensors.ImageComparisonSensors()
    def setUp(self):
        print("Currently setting up")
        self.seventyFiveHealthImage = ConvertImage.convertImageIfRequired(cv2.imread("../../images/testing/health_sensor/seventy-five.jpg"))
        self.oneHundredHealthImage = ConvertImage.convertImageIfRequired(cv2.imread("../../images/testing/health_sensor/one-hundred.jpg"))


        '''
        self.zerohealthimage = cv2.imread("") 
        self.twentyfivehealthimage = cv2.imread("")
        self.fiftyhealthimage = cv2.imread("")
        '''

        
    '''
    def testZeroPercentHealth(self):
        returnedHealth = self.healthSensor.getHealth(self.zerohealthimage)
        actualHealth = 0
        return ( (health - healthSensitivityThreshold) < health < (health + healthSensitivityThreshold) )

    def testTwentyFivePercentHealth(self):
        returnedHealth = self.healthSensor.getHealth(self.twentyfivehealthimage)
        actualHealth = 25
        return ( (health - healthSensitivityThreshold) < health < (health + healthSensitivityThreshold) )

    def testFiftyPercentHealth(self):
        returnedHealth = self.healthSensor.getHealth(self.fiftyhealthimage)
        actualHealth = 50
        return ( (health - healthSensitivityThreshold) < health < (health + healthSensitivityThreshold) )
	'''
    def testSeventyFivePercentHealth(self):
        actualHealth = 75
        returnedHealth = self.healthSensor.getGlobePercentage(self.seventyFiveHealthImage)
        upperBound = returnedHealth < actualHealth + healthSensitivityThreshold
        lowerBound = returnedHealth > actualHealth - healthSensitivityThreshold
        self.assertTrue( upperBound and lowerBound )
    
    def testOneHundredPercentHealth(self):
        actualHealth = 100
        returnedHealth = self.healthSensor.getGlobePercentage(self.oneHundredHealthImage)
        upperBound = returnedHealth < actualHealth + healthSensitivityThreshold
        lowerBound = returnedHealth > actualHealth - healthSensitivityThreshold
        self.assertTrue( upperBound and lowerBound )
        
    def tearDown(self):
        pass
    