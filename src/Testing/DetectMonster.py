'''
Created on 23 May 2014

@author: Gabriel
'''
import unittest, cv2
import math
from ImageManipulation import ImageDetection.ImageDetection


class Test(unittest.TestCase):
    imageDetector = ImageDetection()
    imageContainingMonster = None # TODO - acquire monster image containing fallen.
    monsterImage = imageDetector.convertImageIfRequired(cv2.imread("Testing/images/monsters/Fallen.png"))
    coordsOfMonsterInImage = (10,10)

    def detectFallenMonster(self):
        returnedCoords = self.imageDetector.getCentreCoordsImage(self.monsterImage, self.targetImage)
        absoluteDifference = math.sqrt((returnedCoords - self.coordsOfMonsterInImage) * (returnedCoords - self.coordsOfMonsterInImage)) 
        return absoluteDifference < (50,50)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()