'''
Created on 2 Jun 2014
At a tick, the controller will take a frame and determine whether the player is low on health, drinking a health potion
if this is the case. It will also listen for user input at this take in order to update the health model.
@author: Gabriel
'''
# Initialization of everything required by this module to operate.
import win32com.client 
from Utility import detectKeyboardInputs
from View import ImageComparisonSensors
from PotionDrinker import PotionDrinker
import Globals.Globals
autoit = win32com.client.Dispatch("AutoItX3.Control") # Enables referencing of autoit methods, e.g. autoit.Send()
detectKeyboard = detectKeyboardInputs.detectKeyboardInput() # Enables module to detect keyboard input.

class HealthManaController():
    def __init__(self, model):
        # user should edit to preference
        self.healthDrinkBelowPercentage = 80 
        self.manaDrinkBelowPercentage = 80
        self.healthSensor = ImageComparisonSensors.ImageComparisonSensor(Globals.Globals.globeType.HEALTH)
        self.manaSensor = ImageComparisonSensors.ImageComparisonSensor(Globals.Globals.globeType.MANA)
        self.potionsModel = model
        self.potionDrinker = PotionDrinker(self.potionsModel)
        
        # These store the health and mana values of the last five ticks.
        # This is used to determine if a health or mana potion has already been drunk.
        # These are initialized to 100 - it's intial value doesn't actually matter, as many ticks will be taken.
        self.lastTickManas = [100,100,100,100,100]
        self.lastTickManas = [100,100,100,100,100] 
        
    # This will drink a health potion if the frame passed in indicates the player is on low health and a mana potion if the player is on low mana.
    def tick(self, frame):
        self.healthTick(frame)
        self.manaTick(frame)
        
    def manaTick(self,frame):
        currentTickMana = self.manaSensor.getGlobePercentage(frame)
        manaDecreasing = True
        for tickMana in self.lastTickManas:
            if currentTickMana >= tickMana:
                manaDecreasing = False
        if(currentTickMana < self.manaDrinkBelowPercentage and manaDecreasing):
            self.potionDrinker.drinkPotion("MANA")
        # Upon a tick, listen for user input on whether to update model or provide additional information
        if (detectKeyboard.isEPressed()):
            self.printCurrentMana(frame)
        if (detectKeyboard.isWPressed()):
            self.manaSensor.takeNewFullSample(frame)
        # This will shift all the ticks along, to carry this through to the neck tick.
        self.lastTickManas[0] = currentTickMana
        for index,tickMana in list(enumerate(self.lastTickManas)):
            if (index + 1 < len(self.lastTickManas)):
                self.lastTickManas[index+1] = tickMana

    def healthTick(self,frame):
        currentTickHealth = self.healthSensor.getGlobePercentage(frame)
        healthDecreasing = True
        for tickHealth in self.lastTickManas:
            if currentTickHealth >= tickHealth:
                healthDecreasing = False
        if(currentTickHealth < self.healthDrinkBelowPercentage and healthDecreasing):
            self.potionDrinker.drinkPotion("HEALTH")
        # Upon a tick, listen for user input on whether to update model or provide additional information
        if(detectKeyboard.isRPressed()):
            self.setFullPotions()
        if(detectKeyboard.isShiftKeyPressed()):
            print(self.potionsModel.belt)
        if (detectKeyboard.isEPressed()):
            self.printCurrentHealth(frame)
        if (detectKeyboard.isWPressed()):
            self.healthSensor.takeNewFullSample(frame)
        # This will shift all the ticks along, to carry this through to the neck tick.
        self.lastTickManas[0] = currentTickHealth
        for index,tickHealth in list(enumerate(self.lastTickManas)):
            if (index + 1 < len(self.lastTickManas)):
                self.lastTickManas[index+1] = tickHealth
    ''' TODO
    def setFullManaPotions(self):
        self.potionsModel.setAllPotionsToMana()
    ''' 
    def setFullPotions(self):
        self.potionsModel.refillPotions()
        print("Potions succesfully refilled.")
    def printPotionsModel(self):
        print(self.potionsModel.belt)
        
    def printCurrentHealth(self, frame):
        print("The current health is" + str(self.healthSensor.getGlobePercentage(frame)))
        
    def printCurrentMana(self, frame):
        print("The current mana is" + str(self.manaSensor.getGlobePercentage(frame)))