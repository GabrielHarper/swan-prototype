'''
Created on 25 Apr 2014

@author: Gabriel

This is a development tool. 

This controller is used for viewing and executing various parts of the program. 
For instance, it may be used to view all the images extracted
during determining the player's current health.
This gives us visual cues during the computer vision process.


'''
import win32gui, DiabloWindowException, win32api

# If the Diablo II window cannot be found, raise a DiabloNotRunning exception.
if (win32gui.FindWindow(None,'Diablo II') == 0):
    raise DiabloWindowException.DiabloNotRunning("Window not found")
# External imports
import os
from ImageManipulation import ImageCapture, ImageDetection, ConvertImage
from View import DisplayFrame
import GUIs
screenshotter = ImageCapture.ScreenShotter()
displayFrame = DisplayFrame.DisplayFrame()
screenShotter = ImageCapture.ScreenShotter()
import cv2


def gabsloop():
    _,currentTickFrame = screenshotter.screenshot()
    print("line 41")
    processedFrame = examiner.getResultFrame(currentTickFrame)
    displayFrame.displayFrame(processedFrame, 'processed frame')
    

if __name__ == '__main__':
    function = cv2.matchTemplate
    target = cv2.imread('C:\Users\Gabriel\workspace\DiabloBlackSwan\src\images\monsters\Fallen.png')
    nonEnumParams = target
    enumOptions = (cv2.TM_CCOEFF,cv2.TM_CCOEFF_NORMED,cv2.TM_CCORR,cv2.TM_CCORR_NORMED)
    enumNames = ("cv2.TM_CCOEFF","cv2.TM_CCOEFF_NORMED","cv2.TM_CCORR","cv2.TM_CCORR_NORMED")
    examiner = GUIs.ExamineEnumFunction(function,nonEnumParams,enumOptions,enumNames)
    examiner.executeEvery(gabsloop,1000)
    examiner.parentWindow.mainloop()
    
    while(True):
        _,currentTickFrame = screenshotter.screenshot()
        print("line 41")
        processedFrame = examiner.getResultFrame(currentTickFrame)
        displayFrame.displayFrame(processedFrame, 'processed frame')
