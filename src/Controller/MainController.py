'''
Created on 2 Jun 2014

@author: Gabriel
'''
import DiabloWindowException, win32gui
# If the Diablo II window cannot be found, raise a DiabloNotRunning exception before continuing.
if (win32gui.FindWindow(None,'Diablo II') == 0):
    raise DiabloWindowException.DiabloNotRunning("Window not found")

import cv2, win32gui
import Globals.Globals
from View import DisplayFrame
import AttackController
import HealthManaController
from Model import PotionsModel

from ImageManipulation import ImageCapture, ImageDetection, ConvertImage

screenshotter = ImageCapture.ScreenShotter()
imageDetector = ImageDetection.ImageDetection()
displayFrame = DisplayFrame.DisplayFrame()
potionSetup = PotionsModel.potionSetups.HALFHALF
potionModel = PotionsModel.PotionsModel(potionSetup)
healthManaController = HealthManaController.HealthManaController(potionModel)

while(True):
    if (win32gui.GetForegroundWindow() == Globals.Globals.diabloWindowHandle):
        _,currentTickFrame = screenshotter.screenshot()
        displayFrame.displayFrame(currentTickFrame)
        healthManaController.tick(currentTickFrame)
        AttackController.tick(currentTickFrame)