'''
Created on 2 Jun 2014

@author: Gabriel
'''
from Utility import detectKeyboardInputs
detectKeyboard = detectKeyboardInputs.detectKeyboardInput() # Enables module to detect keyboard input.
import win32com.client
autoit = win32com.client.Dispatch("AutoItX3.Control") # Enables referencing of autoit methods, e.g. autoit.Send()
import Model.AttackSequencesModel
def tick(frame):
    clearScreenAttackSequence = Model.AttackSequencesModel.getClearScreenAttackSequence()
    if(detectKeyboard.isQPressed()):
        attack(clearScreenAttackSequence)
        
def attack(AttackSequence):
    for coordinate in AttackSequence:
        autoit.MouseClick("right",coordinate[0],coordinate[1])
        autoit.sleep(120)