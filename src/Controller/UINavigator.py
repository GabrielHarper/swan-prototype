'''
Created on 31 Dec 2013

@author: Gabriel
'''
import win32com.client, string, random  # External modules
from ImageManipulation import imageProcessing

autoit = win32com.client.Dispatch("AutoItX3.Control") # Enables referencing of autoit methods, e.g. autoit.Send()
class UINavigator:
    '''
    classdocs
    '''

    def __init__(self, screenWidth = 1366, screenHeight = 768):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight
                
    def inputRandomString(self, size=10, chars=string.ascii_uppercase):
        for __ in range(size):
            autoit.Send("#" + random.choice(chars))
            
    def startDiablo(self):
        delayStart = 3000
        print("Bot will start in " + str(delayStart)[0] + " seconds.")
        autoit.Sleep(delayStart)
        autoit.Run("C:\Program Files (x86)\Diablo II\Diablo II.exe")
        autoit.WinWait("Diablo II")
        # This while loop will keep on clicking until the battle.net button appears on screen - this skips through the cinematics.
        maxVal = 0
        while (maxVal < 0.9 ):
            autoit.MouseClick("Left",0,0)
            maxVal,__ = ImageManipulation.imageProcessing.findImage('Images/Buttons/BNetButton.png')
            
    def logOntoBNet(self):
        ImageManipulation.imageProcessing._clickOnImage('Images/Buttons/BNetButton.png')
        ImageManipulation.imageProcessing._waitUntilImageAppears('Images/Buttons/accountSettings.png') # Waits until Bot is at the login screen.
        autoit.Send("#jagercode900 {ENTER}") # Input password and press enter. 
        
    def chooseCharacter(self, charNameImage):   
        ImageManipulation.imageProcessing._waitUntilImageAppears('Images/Characters/JesSee.png')
        ImageManipulation.imageProcessing._clickOnImage('Images/Characters/JesSee.png')
        autoit.Send("{ENTER}")
        
    def createRandomGame(self):
        ImageManipulation.imageProcessing._waitUntilImageAppears('Images/Buttons/CreateGameButton.png')
        ImageManipulation.imageProcessing._clickOnImage('Images/Buttons/CreateGameButton.png')
        self.inputRandomString()
        autoit.Send("{TAB}")
        self.inputRandomString()
        autoit.Send("{ENTER}")
        
    
    def _clickOnImage(self, image): # This code will click on the centre of the given image. 
        __, location = _findImageCurrentScreen(image)
        img = cv2.imread(image)
        x,y = location # maxLoc returns a tuple of the top-left of the image.
        height, width = img.shape[:-1] # Gets the height and width of the image.
        x = x + (width  / 2) # Modifies x to be at the centre of the image.
        y = y + (height / 2) # Modifies y to be at the centre of the image.
        autoit.MouseClick("Left",x,y) # Clicks in the centre of the image.




