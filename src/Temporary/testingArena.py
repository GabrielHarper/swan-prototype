'''
Created on 3 Jan 2014

@author: Gabriel
'''



import win32com.client, win32api
import cv2
import PIL
import ImageGrab
import string
import random
import timeit
import numpy 
autoit = win32com.client.Dispatch("AutoItX3.Control") 

def printInfo(image):
    print("The image's Python type is " +  str(type(image)))
    print("The image's dtype is " + str(image.dtype))
    print("The image's shape is " + str(image.shape))
    

imageOne = cv2.imread("C:\\Users\\Gabriel\\workspace\\DiabloBlackSwan\\src\\Images\\Health\\fullHealth.bmp")
imageTwo = cv2.imread("C:\\Users\\Gabriel\\workspace\\DiabloBlackSwan\\src\\Images\\Health\\halfHealthReducedSize.png")
randPixel = random.randrange(255)
percent = 10
print(imageOne.shape[1] / 100)
widthChange = ( percent * (imageOne.shape[1] / 100))
heightChange = (percent * (imageTwo.shape[0] / 100))
print(widthChange)
print(heightChange)
imageTwo
printInfo(imageOne)
printInfo(imageTwo)

bitwiseAnd = cv2.bitwise_and(imageOne,imageTwo)
cv2.imshow('Bitwise',bitwiseAnd)
absDifference = cv2.absdiff(imageOne,imageTwo)
print(type(absDifference))
sumOfArray = cv2.sumElems(absDifference)
print(absDifference)
print(108*255)
print(sumOfArray)

