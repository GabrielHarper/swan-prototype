'''
Created on 8 May 2014

@author: Gabriel
'''
from Utility.detectKeyboardInputs import detectKeyboardInput
import PerformanceMeasurement
import Tkinter
import tkMessageBox
import View
import cv2
from ImageManipulation import ImageCapture
from ImageManipulation import ImageDetection
from Utility.detectKeyboardInputs import detectKeyboardInput


'''
This class is used to examine an image preprocessing function which takes a frame.
This class can then be used to change it's parameters via a GUI.
We can then use these paramters to apply the function and return the frame. 
We can also display the frame.
'''    

class ExamineEnumFunction(object):
    def __init__(self, f, nonEnumParams, enumOptions, enumNames):
        # Initialize the current Enum to be the first option.
        self.currentEnum = enumOptions[0]
        # Create the parent window which widgets will be attached to. 
        self.parentWindow = Tkinter.Tk()
        # Assign the parameters which will not be changed - these are fixed - except the input frame.
        self.nonEnumParams = nonEnumParams
        # Assign enum parameters to self, so they are accessible in instance functions.
        self.enumOptions = enumOptions
        self.enumNames = enumNames
        # Class used for displaying frames
        self.displayFrame = View.DisplayFrame.DisplayFrame()
        self.imageDetector = ImageDetection.ImageDetection()
        self.detectKeyboard = detectKeyboardInput()
        self.f = f 
        # Initialize the arrays of ExamineEnumFunction buttons (for choosing an enum) and commands (for updating the enum).
        self.radioButtons = []
        self.commands = []
        # button to print out the current enum
        self.button = Tkinter.Button(master=self.parentWindow,text="print enum",command=self.printCurrentEnum)
        self.button.pack()
        # Buttons to stop and start visual output.
        # Used to check whether to stop displaying visual output.
        self.stop = False
        # Make the commands for updating an enum, the radio buttons for choosing an enum and the labels for each enum option.
        self.makeRadioCallbacks()
        self.makeEnumRadios()
        self.makeLabels()
        # pack everything up into the parentWindow
        self.packUp()
    
    def stop(self):
        self.stop = True
        
    def makeUpdateEnumMethod(self, val):
        #This callback will make the current enum variable equal to the value passed. 
        def updateEnumMethod():
            self.currentEnum = val
                    
        return updateEnumMethod
    
    
    def makeRadioCallbacks(self):
        # These callbacks will update the current enum to the value of the ExamineEnumFunction button selected.
        for value in self.enumOptions:
            callback = self.makeUpdateEnumMethod(value)
            self.commands.append(callback)

        
    def makeEnumRadios(self):
        index = 0
        v = Tkinter.IntVar() # not currently sure why this is necessary
        for enum in self.enumOptions:
            currentCommand = self.commands[index];
            currentButton = Tkinter.Radiobutton(master=self.parentWindow, variable = v, value=enum,command=currentCommand)
            self.radioButtons.append(currentButton)
            index = index+1
            

    def printCurrentEnum(self):
        print "Current enum is now" + str(self.currentEnum)
    def getCurrentEnum(self):
        return self.currentEnum
    
    def getResultFrame(self, inputFrame):
        actualParams = []
        actualParams.append(inputFrame)
        #Broken maybe when there is only one param
        for nonEnum in self.nonEnumParams:
            actualParams.append(nonEnum)
        actualParams.append(self.getCurrentEnum())
        result = self.f(actualParams)
        return result
    

    def executeEvery(self, f, milliseconds):
        executor = TkinterExecutor(self.parentWindow, f, milliseconds)
        executor.run();
    
    
        
    def makeLabels(self):
        #Make the labels for each ExamineEnumFunction button, detailing it's value. 
        self.labels = []
        for name in self.enumNames:
            self.labels.append(Tkinter.Label(master=self.parentWindow, text=name))
            
    def packUp(self):
        # Pack the Tkinter labels, radios into the parentWindow
        currentRow = 0
        for radio in self.radioButtons:
            radio.pack()
            currentLabel = self.labels[currentRow]
            currentLabel.pack()
            currentRow = currentRow + 1




class TkinterExecutor:
    def __init__(self, parentWindow,f,milliseconds):
        self.f = f
        self.parentWindow = parentWindow
        self.milliseconds = milliseconds
    
    def run(self):
        self.f()
        self.parentWindow.after(1000, self.run)
    



class ExamineFunction(object):
    detectInput = detectKeyboardInput()


    # Initialize the examiner with the names of the paramters being adjusted and their initial values.
    def __init__(self, f, paramNames, initialParams):
        self.f = f
        self.parentWindow = Tkinter.Tk() # Create a parent window.
        self.paramValues = initialParams
        self.labels = self._createParamNameLabels(paramNames) 
        self.entries = self._createEntries(len(paramNames))
        self.getEntryFunctions = self.createGetEntryFunctions()
        self.buttons = self.createPrintValueButtons()
        self.packEverythingUp()
        executionButton = Tkinter.Button(self.parentWindow, text= "Execute function", command=self.executeFunction)
        executionButton.grid(column=0, row=(len(paramNames) + 1))
        timeItButton = Tkinter.Button(self.parentWindow, text = "Time function", command = self.timeFunction)
        timeItButton.grid(column=1, row=(len(paramNames) + 1) )
        
        
    def packEverythingUp(self):
        for x in range (0, len(self.labels)): # For every parameter, insert the appropriate label, entry and button.
            self.labels[x].grid(column=0, row = x)
            self.entries[x].grid(column=1, row = x)
            self.buttons[x].grid(column=2, row = x)
            
    def getParams(self):
        return self.paramValues
    
    def executeFunction(self):
        self.f(self.getParams()) # Execute the function.
    
    def _createParamNameLabels(self, paramNames): # Used to show which variables the user is editing.
        # These arrays will hold the parameter names and values.
        paramLabels = []
        paramTextRepresentations = []
        # Create a Tkinter-friendly String value for each parameter name and use these as labels for the parent window.
        for paramName in paramNames:
            paramTextRepresentations.append(Tkinter.StringVar())
            paramTextRepresentations[-1].set(paramName)
            paramLabels.append(Tkinter.Label(self.parentWindow,textvariable=paramTextRepresentations[-1])) # append the label
        return paramLabels
    
    def _createEntries(self, numEntries): # Used to create entries through which the user can edit parameters. 
        entries = [] # An array of Tkinter widgets which will be used to edit params
        for _ in range(0,numEntries):
            entries.append(Tkinter.Entry(self.parentWindow))
        return entries
    
    def _createEnumDropdowns(self, numEnumDropdowns):
        pass
    def makeGetEntryFunction(self,entry):
            def getEntryValue():
                indexOfEntry = self.entries.index(entry)
                self.paramValues[indexOfEntry] = entry.get()
                return (entry.get())
            return getEntryValue
    
    def createGetEntryFunctions(self):
        getEntryFunctions = []
        for entry in self.entries:
            getEntryFunctions.append(self.makeGetEntryFunction(entry))
        return getEntryFunctions
    
    def createPrintValueButtons(self):
        buttons = []
        for x in range(0,len(self.getEntryFunctions)):
            buttons.append(Tkinter.Button(self.parentWindow, text="Update value", command=self.getEntryFunctions[x]))
            buttons[x].grid(column=2, row=x)
        return buttons
    
    
    def timeFunction(self):
        seconds = PerformanceMeasurement.timeIt(self.f, 10, self.paramValues)
        tkMessageBox.showinfo('Function execution time', "This function took " + str(seconds) + " seconds to execute.")
                           
    def startWindowLoop(self):
        self.parentWindow.mainloop(1000)
        self.parentWindow.after(1000, self.startWindowLoop)
