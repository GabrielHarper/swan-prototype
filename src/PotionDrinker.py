'''
Created on 2 Jun 2014
This potion drinker takes a model of potions. Upon calling the drinkPotion method,
it will iterate through the model until it finds the correct potion. It will then drink it.

TODO: Update the model upon drinking a potion. 
@author: Gabriel
'''
import win32api, win32com.client
autoit = win32com.client.Dispatch("AutoItX3.Control") # Enables referencing of autoit methods, e.g. autoit.Send()
class PotionDrinker():

    def __init__(self, potionModel):
        self.potionsModel = potionModel # Takes a potion model which the drinker will manipulate. 
        pass
    
    def drinkPotion(self,potionType):
        for beltColumnIndex, beltColumn in enumerate(list(self.potionsModel.belt)): # Goes through every belt column
            for potionIndex, potion in enumerate(list(beltColumn)): # Goes through every slot in the belt column.
                if potion == potionType:
                    autoit.Send(( "0" + str(beltColumnIndex+1)) [1]) # Uses autoit to drink the potion. The [1] flag signals raw input.
                    win32api.Beep(500,500) # Informs user that a potion has been drunk via a beep.
                    beltColumn[potionIndex] = "EMPTY" # Sets that potion slot to be empty.
                    print("I just drank a " + potionType + " potion from belt column " + str(beltColumnIndex + 1) + " and potion slot " + str(potionIndex + 1)) #inform user
                    return # Exit function after drinking health potion.
            if (beltColumnIndex == 3): # If this is the last belt column, inform user. TODO: smart inform
                print("Out of " + potionType + " potions! Please refill as soon as possible and then update the model that you have refilled. :-)")
