        
        
'''
Created on 3 Jun 2014

@author: Gabriel
'''
import cv2, numpy
# converts image to a numpy.ndarray if this isn't already the case.
def convertImageIfRequired(image):
    if isinstance(image, numpy.ndarray):
        return image
    else:
        return cv2.imread(image)
