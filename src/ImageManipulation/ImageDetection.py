'''
Created on 2 May 2014

@author: Gabriel
'''
import cv2, numpy
import View.DisplayFrame
import ImageCapture
class ImageDetection(object):
    displayFrame = View.DisplayFrame.DisplayFrame()
    screenShotter = ImageCapture.ScreenShotter()
    '''
    classdocs
    '''


    def __init__(self):

        '''
        Constructor
        '''

        
        
    def findImage(self, targetImage, imageContainingTarget = screenShotter.screenshot()):
        target = self.convertImageIfRequired(targetImage)
        result = cv2.matchTemplate(imageContainingTarget, target, cv2.TM_CCOEFF_NORMED) 
        __,maxVal,__,maxLoc = cv2.minMaxLoc(result)
        return maxVal, maxLoc # confidence, location
    
    
    def HSBTripleMergeTemplateMatch(self, imageContainingTarget, targetImage, cvMethod):
        target = self.convertImageIfRequired(targetImage)
        target = cv2.cvtColor(target, cv2.COLOR_RGB2HSV)
        self.displayFrame.displayFrame(imageContainingTarget, 'imagecontainingTarget unconverted')
        imageContainingTarget = cv2.cvtColor(imageContainingTarget, cv2.COLOR_RGB2HSV)
        self.displayFrame.displayFrame(imageContainingTarget, 'imagecontainingTarget converted')
        targetHue,targetSaturation,targetValue = cv2.split(target)
        imageContainingTargetHue, imageContainingTargetSaturation, imageContainingTargetValue = cv2.split(imageContainingTarget)
        hueMatch = cv2.matchTemplate(imageContainingTargetHue,targetHue,cvMethod)
        self.displayFrame.displayFrame(hueMatch, 'hueMatch')
        saturationMatch = cv2.matchTemplate(imageContainingTargetSaturation,targetSaturation,cvMethod)
        self.displayFrame.displayFrame(saturationMatch, 'saturationMatch')
        valueMatch = cv2.matchTemplate(imageContainingTargetValue,targetValue,cvMethod)
        self.displayFrame.displayFrame(valueMatch, 'valueMatch')
        combinationWeighted = cv2.addWeighted(hueMatch,0.5,saturationMatch,0.5,0)
        finalCombinationWeighted = cv2.addWeighted(valueMatch,0.2,combinationWeighted,0.8,0)
        self.displayFrame.displayFrame(finalCombinationWeighted, 'finalCombinationWeighted')

        
        


    # Returns the image with rectangle drawn around the target (using highest confidence value position).
    # Will calculate object detection if no coordinates are given.
    def highlightImage(self, imageContainingTarget, targetImage, location=None): 
        target = self.convertImageIfRequired(targetImage)
        targetWidth = target.shape[1]
        targetHeight = target.shape[0]
        if (location is None):
            result = cv2.matchTemplate(imageContainingTarget, target, cv2.TM_CCOEFF_NORMED) 
            __,__,__,maxLoc = cv2.minMaxLoc(result)
            targetXPosition = maxLoc[0]
            targetYPosition = maxLoc[1]
            cv2.rectangle(imageContainingTarget,(targetXPosition,targetYPosition),(targetXPosition+targetWidth,targetYPosition+targetHeight),(0,255,0),3)
        else:
            cv2.rectangle(imageContainingTarget,(location[0],location[1]),(location[0]+targetWidth,location[1]+targetHeight),(0,255,0),3)
        return imageContainingTarget

    
    
    def _waitUntilImageAppears(self, image):    # This code waits until the given image appears on the screen.
        maxVal = 0
        while (maxVal < 0.9):
            maxVal,__ = self.findImage(image)
            
            
    def getCentreCoordsImage(self, imageContainingTarget, targetImage, location=None):
        target = self.convertImageIfRequired(targetImage)
        targetWidth = target.shape[1]
        targetHeight = target.shape[0]
        if (location is None):
            result = cv2.matchTemplate(imageContainingTarget, target, cv2.TM_CCOEFF_NORMED) 
            __,__,__,maxLoc = cv2.minMaxLoc(result)
            targetXPosition, targetYPosition = maxLoc
            centerX = targetXPosition + (targetWidth / 2)
            centerY = targetYPosition + (targetHeight / 2)
        else:
            centerX = location[0] + (targetWidth / 2)
            centerY = location[1] + (targetHeight / 2)
        return (centerX,centerY)

    