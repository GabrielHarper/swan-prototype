'''
Created on 31 Dec 2013
@author: Gabriel



This class contains various image processing methods. A brief outline is as follows:
- Screenshot returns an image, stored as a numpy array,  which can be manipulated with openCV methods.
- _findImageCurrentScreen
- _waitUntilImageAppears
- _clickOnImage
- 
- 
'''

import win32com.client, cv2, ImageGrab, numpy, win32gui  # External modules
from ImageManipulation import ImageCapture
from Utility import detectKeyboardInputs
import ImageCapture
autoit = win32com.client.Dispatch("AutoItX3.Control") # Enables referencing of autoit methods, e.g. autoit.Send()
detectKeyboard = detectKeyboardInputs.detectKeyboardInput()
diabloWindowHandle = win32gui.FindWindow(None,'Diablo II')
screenshotter = ImageCapture.ScreenShotter()

'''
def verifyImage(image):
    if(image.dtype, image.shape, image.x.... etc. ):
        return True
    else:
        return False
'''




def _screenshotDiabloExplore(topMultiplyConstant, rightDivideConstant):
    left,top,right,bottom = win32gui.GetWindowRect(diabloWindowHandle)
    print(left,top,right,bottom)
    if ((top / topMultiplyConstant) > bottom): # Prevents error of trying to grab a _getRawScreenshot with negative height.
        topMultiplyConstant = 1
        print("topMultiplyConstant too high")
    if ((right / rightDivideConstant) < left): # Prevents error of trying to grab a _getRawScreenshot with negative width.
        rightDivideConstant = 1
        print("rightDivideConstant too high")
    top = int(top * topMultiplyConstant)
    right = int(right / rightDivideConstant)
    healthImagePIL = ImageGrab.grab(bbox=(left,top,right,bottom))
    healthImage =  screenshotter._convertPILToCV(healthImagePIL)
    return healthImage


        

def _getAbsDiff(imageOne,imageTwo): # Compares two images via absolute difference between each pixel value and returning a sum. Images passed must be the same size.
    img1 = imageOne
    img2 = imageTwo
    absDiff = cv2.absdiff(img1,img2)
    sumOfDiff = cv2.sumElems(absDiff)
    return sumOfDiff # Returns a 4-element tuple of differences.


def _focusDiabloWindow():
    diabloWindowHandle = win32gui.FindWindow(None,'Diablo II')
    win32gui.SetForegroundWindow(diabloWindowHandle)