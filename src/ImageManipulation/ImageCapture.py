'''
Created on 17 Apr 2014

@author: Gabriel
'''
import win32gui, ImageGrab, numpy, ConvertImage, cv2

class ScreenShotter(object):
    '''
    classdocs
    '''
    diabloWindowHandle = win32gui.FindWindow(None,'Diablo II')
    def __init__(self):
        pass

    
    '''
    Gets a screenshot for a given window, which defaults to the Diablo II window.  
    The window must be currently active and shown on the current screen.
    This method converts to image into a CV-usable format.
    This method will also only capture the given window, not the entire screen.
    '''
    def screenshot(self, windowHandle = diabloWindowHandle): 
        coordinatesOfScreenshot, rawScreenshot = self._getRawScreenshot(windowHandle)
        cvScreenshot = self._convertPILToCV(rawScreenshot)
        return coordinatesOfScreenshot, cvScreenshot
    
    
    def _convertPILToCV(self, image): # Converts an image into a CV usable format.
        pil_image = image.convert('RGB') 
        cv_image = numpy.array(pil_image) 
        cv_image = cv_image[:, :, ::-1].copy() # Convert from RGB to BGR
        return  cv_image
    
    '''
    Gets a raw screenshot (in PIL format) for the given window handle.
    Returns the screenshot and it's dimensions. 
    '''
    def _getRawScreenshot(self, windowHandle = diabloWindowHandle): 
        left,top,right,bottom = win32gui.GetWindowRect(windowHandle)
        coordinates = (left,top,right,bottom)
        try:
            return coordinates, ImageGrab.grab(bbox=(left,top,right,bottom))
        except: # Bad code, *REQUIRES FIXING*
                # Exceptions to be built in - todo
            print "SCREENSHOT ERROR"