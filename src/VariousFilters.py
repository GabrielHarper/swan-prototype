'''
Created on 17 Jan 2014

@author: Gabriel

Key notes:
-  Unfortunately, cv2 functions often require cv2.waitKey to be called after being called. The cause of this bug is unknown.
'''
import cv2, numpy, timeit, win32com.client, win32gui
from ImageManipulation import imageProcessing
from Utility import detectKeyboardInputs
from ImageManipulation import ImageCapture
from matplotlib import pyplot

detectKeyboardInput = Utility.detectKeyboardInputs.detectKeyboardInput()

#import CaptureVideo
#webcam = CaptureVideo.CaptureVideo()


class Filter(object): # Basic outline of a image filter for a given frame.
    def __init__(self, name):
        self.name = name
    
    def showFrame(self):
        cv2.imshow(self.name, self.filteredFrame) # Displays GUI window with frame
        cv2.waitKey(1)
    
    def updateFrame(self, newFrame):
        self.frame = newFrame
        self.processFrame()
        
class backgroundSubtractMOG(Filter):
    def __init__(self, name, frame):
        Filter.__init__(self, name, frame)
        self.backgroundSubtractor = cv2.BackgroundSubtractorMOG()
        self.filteredFrame = self.backgroundSubtractor.apply(self.frame)
        
class Threshold(Filter): 
    def __init__(self, name, maxThreshValue, thresholdType, initialThresholdValue, adaptive=False, adaptiveType = None):
        Filter.__init__(self,name)
        self.maxThreshValue = maxThreshValue
        self.thresholdType = thresholdType
        self.thresholdValue = initialThresholdValue
        self.adaptive = adaptive
        self.adaptiveType = adaptiveType

    def getFilteredFrame(self):
        return self.filteredFrame
    
    def convertToGray(self):
        self.grayFrame =  cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
        
    def processFrame(self):
        self.convertToGray()
        self.filteredFrame = self.filterFrame()
    
    def filterFrame(self):
        if self.adaptive:
            thresholdFrame = cv2.adaptiveThreshold(self.grayFrame,self.thresholdValue, self.adaptiveType, self.thresholdType, 11, 2)
        else:
            _,thresholdFrame = cv2.threshold(self.grayFrame,self.thresholdValue,self.maxThreshValue, self.thresholdType)
        cv2.waitKey(1)
        return thresholdFrame

    def adjustThresholdValue(self): # Enables adjustment of threshold value in real time.
            if detectKeyboardInput.isAltKeyPressed():
                self.thresholdValue += 1
            if detectKeyboardInput.isShiftKeyPressed():
                self.thresholdValue -= 1


